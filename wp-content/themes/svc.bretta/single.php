<?php
get_header();
require_once('partials/partial.breadcrumbs.php');
?>

<div class="wrapper">
	<div class="container-fluid content news">
		<div class="row">
			<div class="col-lg-12">
				<div class="article">
					<?php if (have_posts()) :  while (have_posts()) : the_post(); ?>
						<div class="title"><?php the_title() ?></div>
						<div class="date"><?php the_time('d.m.Y'); ?></div>
						<?php if(has_post_thumbnail()) :?>
							<div class="img" style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), [920, 715]); ?>');"></div>
						<?php else: ?>
							<img class="title-image" src="<?php bloginfo('template_url'); ?>/assets/img/no-photo-920x715.png" alt="no-photo">
						<?php endif; ?>
						<div class="content">
							<?php if(get_the_content()) {
								the_content();
							}
							else {
								?>
								<div class="tcenter mt30">Контент этой страницы еще не размещен.</div>
								<?php
							}
							?>
						</div>
					<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>