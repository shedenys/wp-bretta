$(function() {
    $("input[type=tel]").mask("+380 (99) 999-99-99");

    $('#callbackForm').submit(function(e){
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            type: 'post',
            dataType: 'json',
            data: $(this).serialize(),
            success: function(data) {
                $('#callbackForm').fadeOut(function(){
                    $('#callbackFormResult').html('Спасибо! Запрос отправлен.');
                });
            },
            error:  function(xhr, str){
                $('#callbackForm').fadeOut(function(){
                    $('#callbackFormResult').html(xhr.responseText);
                });
            }
        });
    });
});