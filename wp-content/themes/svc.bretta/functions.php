<?php
require_once('functions/function.dimox_breadcrumbs.php');

/*
 * Подключение стилей и скриптов
 */
function loadStylesScripts() {
	wp_enqueue_style('main', get_template_directory_uri().'/assets/css/main.css');
	wp_enqueue_style('open-sans-google-fonts', get_template_directory_uri().'/assets/css/open-sans-google-fonts.css');
	wp_enqueue_script('jquery-custom', get_template_directory_uri().'/assets/js/jquery.min.js');
	wp_enqueue_script('magnific-popup', get_template_directory_uri().'/assets/js/jquery.magnific-popup.min.js', null, false, true);
}

/*
 * Загрузка скриптов и стилей
 */
add_action('wp_enqueue_scripts', 'loadStylesScripts');

/*
 * Поддержка миниатюр
 */
add_theme_support('post-thumbnails');
set_post_thumbnail_size(180, 180);
set_post_thumbnail_size(920, 715);

/*
 * Виджеты хедера
 */
// Логотип
register_sidebar([
	'name' => 'Логотип в хедере',
	'id' => 'header_logo',
	'description' => 'Вставьте код картинки с помощью виджета "HTML-код"',
	'before_widget' => '',
	'after_widget' => ''
]);
// Номер телефона
register_sidebar([
	'name' => 'Номер телефона в хедере',
	'id' => 'header_phone',
	'description' => 'Вставьте номер телефона с помощью виджета "HTML-код"',
	'before_widget' => '',
	'after_widget' => ''
]);

/*
 * Виджет Google карты
 */
register_sidebar([
	'name' => 'Карта местоположения',
	'id' => 'location_map',
	'description' => 'Вставьте код карты с помощью виджета "HTML-код"',
	'before_widget' => '',
	'after_widget' => ''
]);

/*
 * Виджеты в футера
 */
// Логотип
register_sidebar([
	'name' => 'Логотип в футере',
	'id' => 'footer_logo',
	'description' => 'Вставьте код картинки с помощью виджета "HTML-код"',
	'before_widget' => '',
	'after_widget' => ''
]);
// Email
register_sidebar([
	'name' => 'Email в футере',
	'id' => 'footer_email',
	'description' => 'Вставьте код с помощью виджета "HTML-код"',
	'before_widget' => '',
	'after_widget' => ''
]);
// Телефоны
register_sidebar([
	'name' => 'Телефоны в футере',
	'id' => 'footer_phones',
	'description' => 'Вставьте код с помощью виджета "HTML-код"',
	'before_widget' => '',
	'after_widget' => ''
]);
// Адрес
register_sidebar([
	'name' => 'Адрес в футере',
	'id' => 'footer_address',
	'description' => 'Вставьте код с помощью виджета "HTML-код"',
	'before_widget' => '',
	'after_widget' => ''
]);
// Skype
register_sidebar([
	'name' => 'Skype в футере',
	'id' => 'footer_skype',
	'description' => 'Вставьте код с помощью виджета "HTML-код"',
	'before_widget' => '',
	'after_widget' => ''
]);

/*
 * Виджет "О компании" с главной страницы
 */
register_sidebar([
	'name' => 'Блок "О компании"',
	'id' => 'home_about_company',
	'description' => 'Вставьте код с помощью виджета "HTML-код"',
	'before_widget' => '',
	'after_widget' => ''
]);

/*
 * Слайдер
 */
register_nav_menu('menu', 'Main Menu');

function slidesPosts() {
	$labels = [
		'name' => 'Слайды',
		'singular_name' => 'Слайд',
		'add_new' => 'Добавить новый',
		'add_new_item' => 'Добавить новый слайд',
		'edit_item' => 'Редактировать слайд',
		'new_item' => 'Новый слайд',
		'view_item' => 'Посмотреть слайд',
		'search_items' => 'Найти слайд',
		'not_found' => 'Слайд не найден',
		'not_found_in_trash' => 'В корзине слайда не найдено',
		'parent_item_colon' => '',
		'menu_name' => 'Слайды'
	];
	$args = [
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => null,
		'menu_icon' => 'dashicons-images-alt',
		'supports' => ['title', 'thumbnail', 'custom-fields', 'page-attributes']
	];
	register_post_type('slides', $args);
}
add_action('init', 'slidesPosts');
// Добавляем требуемые размеры миниатюры
set_post_thumbnail_size(1920, 570);

/*
 * Обработка обратного звонка
 */
function prefix_send_email_to_admin() {

	// Если получено от формы имя и телефон
	if (!empty($_POST['name']) && !empty($_POST['phone'])) {
		// Формируем письмо
		$mail = 'Поулчен новый запрос на обратный звонок!<br>Имя: '.$_POST['name'].'<br>Телефон: '.$_POST['phone'].'<br>Дата и время: '.date('m.d.Y H:i:s');
		// Получатель письма
		$to = get_option('admin_email');
		$headers = 'From: Bretta <noreply@bretta.com>' . "\r\n";
		// Отправялем письмо
		wp_mail($to, 'Поулчен запрос на обратный звонок', $mail, $headers);
		// Формируем ответ
		echo 'Спасибо! Запрос отправлен.';
	}
	else {
		// Формируем ответ
		echo 'Ошибка! Не все данные были получены.';
	}
}
add_action( 'admin_post_nopriv_contact_form', 'prefix_send_email_to_admin' );
add_action( 'admin_post_contact_form', 'prefix_send_email_to_admin' );