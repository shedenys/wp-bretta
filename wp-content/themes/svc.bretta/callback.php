<?php
/*
Template Name: Шаблон страницы обратного звонка
 */
?>
<div class="white-popup mfp-with-anim callback-popup">
	<div class="head">Заказать звонок</div>
	<div class="body">
		<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" id="callbackForm">
			<div class="item">
				<label>Имя</label>
				<input type="text" name="name" required>
			</div>
			<div class="item">
				<label>Телефон</label>
				<input type="tel" name="phone" required>
			</div>
			<div class="item tcenter">
				<button class="btn">Заказать</button>
			</div>
			<input type="hidden" name="action" value="contact_form">
		</form>
		<div id="callbackFormResult"></div>
	</div>
</div>
<script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.maskedinput.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/js/callback.js"></script>

<?php
print_r($_POST);
//exit;
?>