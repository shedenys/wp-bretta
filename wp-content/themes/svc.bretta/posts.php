<?php
/*
Template Name: Шаблон страницы новостей
 */
get_header();
require_once('partials/partial.breadcrumbs.php');
?>

<div class="wrapper">
	<div class="container-fluid content news">
		<?php $posts = new WP_Query(['post_type' => 'post', 'order' => 'desc']); ?>
		<?php if ($posts->have_posts()) : while($posts->have_posts()) : $posts->the_post(); ?>
			<div class="custom-block-1 custom-block item">
				<div class="row">
					<div class="col-lg-8">
						<div class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
						<div class="date"><?php the_time('d.m.Y'); ?></div>
						<div class="description"><?php the_excerpt(); ?></div>
						<a href="<?php the_permalink(); ?>" class="more">Подробнее</a>
					</div>
					<div class="col-lg-1"></div>
					<div class="col-lg-3">
						<a href="<?php the_permalink(); ?>">
							<?php if(has_post_thumbnail()) :?>
								<?php the_post_thumbnail([920, 715], ['class' => 'title-image']); ?>
							<?php else: ?>
								<img class="title-image" src="<?php bloginfo('template_url'); ?>/assets/img/no-photo-920x715.png" alt="no-photo">
							<?php endif; ?>
						</a>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
		<div class="page-pagination">
			<div class="row">
				<div class="col-sm-6">
					<div class="pagination">
						<?php
						// global $wp_query;
						$big = 999999999; // need an unlikely integer

						echo paginate_links( array(
							'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							'format' => '?paged=%#%',
							'current' => max( 1, get_query_var('paged') ),
							'total' => $posts->max_num_pages,
							'prev_text' => '&laquo;',
							'next_text' => '&raquo'
						) );
						?>
					</div>
				</div>
			</div>
		</div>
		<?php else: ?>
			<div class="tcenter mt30">Нет доступных новостей.</div>
		<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>
