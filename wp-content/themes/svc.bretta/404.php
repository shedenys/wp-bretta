<?php
get_header();
require_once('partials/partial.breadcrumbs.php');
?>

	<div class="wrapper">
		<div class="container-fluid content news">
			<div class="row">
				<div class="col-lg-12">
					<div class="article">
						<div class="title">Ошибка 404</div>
						<div class="content">
							<div class="tcenter mt30">Страница не найдена</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>