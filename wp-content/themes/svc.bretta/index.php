<?php get_header(); ?>

    <?php
    $slides = new WP_Query([
        'post_type' => 'slides',
        'posts_per_page' => -1, // отключаем учет пагинации
        'order' => 'asc'
    ]);
    ?>
    <?php if($slides->have_posts()) : ?>
        <div class="banners">
            <ul id="bannersSlider">
	            <?php while($slides->have_posts()) : $slides->the_post(); ?>
                    <li>
                        <div class="slide">
                            <?php $slideCustomFields = get_post_custom(get_the_ID()); ?>
                            <div class="text">
                                <?php if(isset($slideCustomFields['title'][0]) && $slideCustomFields['title'][0]):?>
                                    <div class="title"><?php echo $slideCustomFields['title'][0]; ?></div>
	                            <?php endif; ?>
	                            <?php if(isset($slideCustomFields['description'][0]) && $slideCustomFields['description'][0]):?>
                                    <div class="paragraph"><?php echo $slideCustomFields['description'][0]; ?></div>
	                            <?php endif; ?>
	                            <?php if(isset($slideCustomFields['link'][0]) && $slideCustomFields['link'][0]):?>
                                    <a href="<?php echo $slideCustomFields['link'][0]; ?>">Подробнее</a>
	                            <?php endif; ?>
                            </div>
                            <div class="img" style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), [1920, 570]); ?>')"></div>
                        </div>
                    </li>
	            <?php endwhile; ?>
            </ul>
            <a href='#' id='bannersSliderGoPrev' onclick='return false' class='slider-controls button left hidden-xs hidden-sm hidden-md'><i class='fa fa-angle-left' aria-hidden='true'></i></a>
            <a href='#' id='bannersSliderGoNext' onclick='return false' class='slider-controls button right hidden-xs hidden-sm hidden-md'><i class='fa fa-angle-right' aria-hidden='true'></i></a>
        </div>
    <?php endif;
    wp_reset_query(); // Сбрасываем результаты WP-цикла

    $latestPost = new WP_Query([
        'posts_per_page' => 1,
        'orderby' => 'desc'
    ]);

    if ($latestPost->have_posts()) :  while ($latestPost->have_posts()) : $latestPost->the_post(); ?>
        <div class="wrapper">
            <div class="container-fluid content">
                <div class="custom-block-1 custom-block">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></div>
                            <div class="description"><?php the_excerpt(); ?></div>
                            <a href="<?php the_permalink(); ?>" class="more">Подробнее</a>
                        </div>
                        <div class="col-lg-1"></div>
                        <div class="col-lg-3">
	                        <?php the_post_thumbnail([920, 715], ['class' => 'title-image']); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
    <?php wp_reset_query(); endif; ?>

    <div class="content">
        <div class="custom-block custom-block-2">
            <div class="wrapper">
                <div class="container-fluid">
                    <div class="row">
	                    <?php if(!dynamic_sidebar('home_about_company')): ?>Виджет "Блок "О компании"<?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="title">Мы на карте</div>
            </div>
        </div>
        <div class="map">
	        <?php if(!dynamic_sidebar('location_map')): ?>Виджет "Карта местоположения"<?php endif; ?>
        </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function() {
        var bannersSlider = $("#bannersSlider").lightSlider({
            item: 1,
            loop: true,
            auto: true,
            pause: 5000,
            pauseOnHover: true,
            speed: 800,
            adaptiveHeight: true,
            controls: false,
            enableDrag: true
        });
        $('#bannersSliderGoPrev').click(function(){
            bannersSlider.goToPrevSlide();
        });
        $('#bannersSliderGoNext').click(function(){
            bannersSlider.goToNextSlide();
        });
    });
</script>
<?php get_footer(); ?>